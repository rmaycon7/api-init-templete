"use strict"
const fs = require('fs')
const path = require('path')
const mkdir = (pathdir = '') => {
    fs.mkdirSync(pathdir, {
        recursive: true
    })
}
const mkFile = (fileNameAndAbsPath = '', data = '') => {
    fs.writeFileSync(fileNameAndAbsPath, data)
}

const checkDirExists = (pathdir) => {
    return fs.existsSync(pathdir)
}

const setIcon = (pathdest) => {
    fs.copyFileSync(path.resolve(__dirname, 'src/data/favicon.ico'), pathdest)
}

// file = fs.createWriteStream()
// file.

module.exports = {
    mkdir,
    mkFile,
    checkDirExists,
    setIcon
}