#!/usr/bin/env node

// const program = require('commander')
// program
//     .usage(`create <name> `)
//     // .option('-p, --peppers', 'Add peppers')
//     // .option('-P, --pineapple', 'Add pineapple')
//     // .option('-b, --bbq-sauce', 'Add bbq sauce')
//     // .option('-c, --cheese [type]', 'Add the specified type of cheese [marble]', 'marble')
//     .version(`${require('../package.json').version}`, '-v,--version', 'Mostra a versão da aplicação.')
//     .option('--offline', 'Instalar pacotes offline.')
//     .parse(process.argv)


const utils = require('./utils')
const path = require('path')
const inquirer = require('inquirer')
const chalk = require('chalk')
const checkName = require('validate-npm-package-name')
let force = false
let appName = ''
let initchoice = ''

const CheckNameIsValid = () => {
    const result = checkName(appName)
    if (!result.validForNewPackages) {
        console.error(chalk.red(`Invalid project name: "${appName}"`))
        result.errors && result.errors.forEach(err => {
            console.error(chalk.red.dim('Error: ' + err))
        })
        result.warnings && result.warnings.forEach(warn => {
            console.error(chalk.red.dim('Warning: ' + warn))
        })
        process.exit(1)
    }

}


const initConfig = () => {
    utils.mkdir(appName)
    const root_app = path.resolve(__dirname, appName)
    utils.setIcon(root_app)
    inquirer
        .prompt([{
            type: 'list',
            name: 'initchoices',
            message: 'Opções de configuração:',
            choices: [{
                    name: `default: ${chalk.yellow('(')}${chalk.bold.green('pastas, arquivos e modulos')}${chalk.yellow(')')}`
                },
                `full: ${chalk.yellow('(')}${chalk.bold.green('pastas, arquivos,  modulos, configuração e upload de arquivos')}${chalk.yellow(')')}`,
                `manual: ${chalk.bold.yellow('(')}${chalk.green('Escolha quais funcionalidades deja.')}${chalk.bold.yellow(')')}`
            ]
        }])
        .then(answers => {
            initchoice = answers.initchoices.split(':')[0]
            // console.log({
            //     c: answers.initchoices.split(':')[0]
            // })
            // console.log(JSON.stringify(answers, null, '  '));
        })
}

// const 



require('yargs')
    .usage('Uso: $0 create <app-name> [options]')

    .option('force', {
        alias: 'f'
    })
    // .command('create', {
    //     alias: 'c'
    // })
    .command({
        command: 'create <app-name> [options]',
        aliases: ['create', 'init'],
        desc: 'Cria uma nova API',
        builder: (yargs) => yargs.default('value', 'true'),
        handler: (argv) => {
            let filepath = path.resolve(__dirname, argv.appName)
            if (utils.checkDirExists(filepath)) {
                inquirer
                    .prompt([{
                        type: 'expand',
                        // type: 'spawn',
                        message: `Conflict com a pasta ${argv.appName}${argv.appName[argv.appName.length-1] =='/'? '':'/'}: `,
                        name: 'overwrite',
                        choices: [{
                                key: 'y',
                                name: 'Overwrite',
                                value: 'overwrite'
                            },
                            {
                                key: 'n',
                                name: 'no',
                                value: 'no'
                            },
                            new inquirer.Separator(),
                            {
                                key: 'x',
                                name: 'Abortar',
                                value: 'abort'
                            }
                        ]
                    }])
                    .then(answers => {
                        if (answers.overwrite == 'overwrite') {
                            force = true
                            appName = argv.appName
                            CheckNameIsValid()
                            initConfig()
                            // console.log("teste")

                        } else if (answers.overwrite == 'no') {
                            inquirer.prompt([{
                                type: 'input',
                                name: 'name',
                                message: "Digite o novo nome do prjeto:",
                            }]).then(data => {
                                appName = data.name
                                CheckNameIsValid()
                                initConfig()

                                // console.log("teste")
                                // console.log({
                                //     appName: path.basename(appName, '/')
                                // });
                            })
                        } else {
                            process.exit(1)
                        }
                        // console.log({
                        //     force: force
                        // });
                        // console.log(JSON.stringify(answers, null, '  '));
                    });
            }
            // console.log(`setting ${argv._} to ${argv.appName} \n `)
            // console.log({
            //     argv: argv
            // });
        }
    })

    // .command({
    //     command: "*",
    //     handler: (yargs) => {
    //         console.log(yargs)
    //     }
    // })
    .describe('f', "Reescrever app caso ja exista um app com mesmo nome.")
    // .describe('c', "Criar um novo app.")
    // .check
    // .showHelpOnFail(true, 'whoops, something went wrong! run with --help')
    // .describe('version', "Exibe a versão do app.")
    .demandCommand(2, 'É necessário pelo menos 2 argumentos, exmplo: api-init create <app-name>').recommendCommands().strict()
    .argv