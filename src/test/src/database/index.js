"use strict"
const mongoose = require("mongoose"),
    {
        db
    } = require("../config/default.config"),
    /* Função que irá se conectar com a base de dados, 
    ira ficar se tentadno se reconectar caso a conexão com banco de dados caia */
    connect = async (mongoose) => {
        try {
            mongoose.Promise = global.Promise
            mongoose.connect(`${db.mode}://${db.url}:${db.port}/${db.name}`)
        } catch (error) {
            setTimeout(() => {
                return con(mongoose)
            }, 2000);
        }
    }

connect(mongoose)
module.exports = mongoose