"use strict"
const app = require("./app/app"),
    utils = require("./utils"),
    os = require("os")


/* atribuindo o valor da porta em uma constante */
utils.config()
const _port = utils.getPort()

/* 
utilizando um modulo para checkar se a porta escolhida esta disponivel para uso, evitando erro de execução
*/


if (utils.getEnv().toLocaleLowerCase() != 'production') {
    const portfinder = require('portfinder'),
        chalk = require('chalk')

    portfinder.getPort({
        port: _port, // minimum port
        stopPort: _port + 500 // maximum port
    }, (error, port) => {
        if (error) throw error
        else {
            app.listen(port, (error) => {
                console.clear()
                console.log(`${chalk.green('		Running in')}
${chalk.red(utils.getIps(port))}`);
            })

        }
    })
} else {
    app.listen(_port, (error) => {
        console.clear()
        console.log(`Running in 
${utils.getIps(_port)}`);
    })
}