"use strict"
const bp = require("body-parser"),
    compression = require("compression"),
    cors = require("cors"),
    express = require("express"),
    errorConf = require("../config/default.errors"),
    helmet = require("helmet"),
    app = express()

/* Iniciando a ativação das configurações da aplicação, como a proteção contra ataques, a configuração dos cabeçalhos. */
app.use(bp.json())
app.use(bp.urlencoded({
    extended: true
}))
app.use(cors())
app.use(helmet())
app.use(compression())
/* A rotas deve ficar a partir daqui
exemplo:
    app.get("/", (req,res, next) => {
        return res.status(202).json({
            msg: "ok"
        })
    })

    ou
    require("./routes")(app)
    neste métodos todos os arquivos de rotas contido no diretório Routes (exceto o index.js que é uma função) serão inportados.
 */

require("./routes/index")(app)
/* Este app.use deve ficar após todas as rotas da aplicação, para o caso de nenhuma rota ser encontrada, ele informará ao requistante dorecurso que este mesmo não existe */
app.use(async (req, res, next) => {
    let error = errorConf.resource_not_found
    error.message = `Rota ${req.method} para "${req.path}" não encontrada, por favor verifique a url ou método fornecido e tente novamente.`
    error.name = `${req.method} ${req.path}`
    return res.status(error.statusCode).json(error)
})
module.exports = app