"use strict"
const mongoose = require("../../database/")
const bcrypt = require("bcryptjs")
const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        bcrypt: true
    }
})

UserSchema.plugin(require("mongoose-bcrypt"), {
    rounds: 10
})

module.exports = mongoose.model("User", UserSchema)