"use strict"
const User = require("../models/User.js"),
    utils = require("../../utils"),
    paths = ["email", "id", "name"],
    defaultResponse = (data, code) => {
        if (data) {
            return {
                data: data,
                statusCode: code
            }

        } else {
            return errorResponse(errorConf.user_not_found)
        }
    },
    errorResponse = (error) => {
        return error
    },
    errorConf = require("../../config/default.errors.js")
module.exports = {
    getAll: () => {
        return User.find()
            .then(data => defaultResponse(utils.serialize(data, 2, paths), 200))
            .catch(error => errorResponse(errorConf.users_not_found))
    },
    getOne: (id) => {
        return User.findById(id)
            .then(data => defaultResponse(utils.serialize(data, 1, paths), 200))
            .catch(error => errorResponse(errorConf.user_not_found))
    },
    create: (data) => {
        return User.create(data)
            .then(data => {
                return defaultResponse(utils.serialize(data, 1, paths), 200)

            })
            .catch(error => {
                if (error.code == 11000) {

                    return errorResponse(errorConf.user_already_exists)
                }
                return errorResponse(errorConf.internal_error)
            })
    },
    update: (data, id) => {
        return User.findOneAndUpdate({
                _id: id
            }, data, {
                new: true
            })
            .then(data => defaultResponse(utils.serialize(data, 1, paths), 202))
            .catch(error => errorResponse(errorConf.user_not_found))
    },
    delete: (id) => {
        return User.findOneAndRemove({
                _id: id
            })
            .then(data => defaultResponse(utils.serialize(data, 1, paths), 202))
            .catch(error => errorResponse(errorConf.user_not_found))
    },
    getbyEmail: (email) => {
        return User.find({
                email: email
            })
            .then(data => {
                paths.push("password")
                return defaultResponse(utils.serialize(data[0], 1, paths), 200)
            })
            .catch(error => errorResponse(errorConf.user_not_found))
    },

}