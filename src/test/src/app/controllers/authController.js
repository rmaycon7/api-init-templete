"use strict"
const User = require("./userController"),
    utils = require("../../utils"),
    paths = ["email", "id", "name", "password"],
    defaultResponse = (data, code) => {
        if (data) {

            return {
                data: data,
                statusCode: code
            }

        } else {
            return errorResponse(errorConf.user_not_found)
        }
    },
    errorResponse = (error) => {
        return error
    },
    errorConf = require("../../config/default.errors.js"),
    jwt = require('jsonwebtoken'),
    {
        secret
    } = require('../../config/secret.json'),
    bcrypt = require("bcryptjs")

module.exports = {
    login: async (email, password) => {
        let data = await User.getbyEmail(email)
        if (data.statusCode == 200) {
            if (await bcrypt.compare(password, data.data.password)) {
                let token = utils.generateToken({
                    user: data.data.id
                })
                data.token = token
                return data
            } else {
                let {
                    password_wrong
                } = require('../../config/default.errors')
                data.data = password_wrong
                data.statusCode = password_wrong.statusCode
                return data
            }
        } else {
            let {
                user_not_found
            } = require('../../config/default.errors')
            return {
                data: user_not_found,
                statusCode: user_not_found.statusCode
            }
        }
    }
}