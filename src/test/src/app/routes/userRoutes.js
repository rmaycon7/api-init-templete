"use strict"
const express = require("express"),
    router = express.Router(),
    userController = require("../controllers/userController"),
    authorization = require('../middlewares/authorization'),
    errors = require('../../config/default.errors')
/*
Rota: Ver todos os usuários
*/
router.get("/", authorization, async (req, res, next) => {
    const response = await userController.getAll()
    return res.status(response.statusCode).json(response)
})
/*
Rota: Ver um usuário
*/
router.get("/:id", authorization, async (req, res, next) => {
    const response = await userController.getOne(req.params.id)
    return res.status(response.statusCode).json(response)
})
/*
Rota: Criar usuário
*/
router.post("/", async (req, res, next) => {
    const response = await userController.create(req.body)
    console.log(response);

    return res.status(response.statusCode).json(response)
})
/*
Rota: Atualizar usuário
*/
router.patch("/:id", authorization, async (req, res, next) => {
    if (req.userId === req.params.id) {
        const response = await userController.update(req.body, req.params.id)
        return res.status(response.statusCode).json(response)
    } else {
        let response = errors.not_authorized
        response.message = 'Id pertence a outro usuário! Tente novamente.'
        response.name = "Id Incorreto!"
        return res.status(response.statusCode).json(response)
    }
})

router.delete("/:id", async (req, res, next) => {
    const response = await userController.delete(req.params.id)
    return res.status(response.statusCode).json(response)
})

module.exports = app => app.use("/users", router)