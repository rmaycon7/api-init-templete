"use strict"
const express = require("express"),
    router = express.Router(),
    auth = require('../controllers/authController'),
    User = require('../controllers/userController'),
    utils = require('../../utils')
router.post('/login', async (req, res, next) => {
    let {
        email,
        password
    } = req.body
    let response = await auth.login(email, password)
    return res.status(response.statusCode).send(response)
})
router.post('/register', async (req, res, next) => {
    const response = await User.create(req.body)
    if (response.statusCode == 200) {
        response.token = utils.generateToken({
            user: response.data.id
        })
    }
    return res.status(response.statusCode).send(response)
})
module.exports = app => app.use("/", router)