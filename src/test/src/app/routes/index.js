"use strict"
const fs = require("fs")
const path = require("path")
/* Criando função para import automaticamente todos os arquivos js colocados neste diretório */
module.exports = app => {
    fs
        .readdirSync(__dirname)
        .filter(file => file.indexOf(".") !== 0 && file !== "index.js" && path.extname(file) === ".js")
        .forEach(file => require(path.resolve(__dirname, file))(app))
}
