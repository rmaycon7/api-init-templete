const jwt = require('jsonwebtoken'),
    {
        secret
    } = require('../../config/secret.json'),
    User = require('../controllers/userController'),
    utils = require('../../utils'),
    errors = require('../../config/default.errors')

module.exports = async (req, res, next) => {
    const header = req.headers.authorization
    let parts
    try {

        parts = header.split(" ")
    } catch (error) {
        parts = ""
    }
    const [scheme, token] = parts
    /* 
    verificando se dentro dos cabeçalhos da requisição contém o tken de autorização, to tipo Bearer token. 
     */
    if (!header || !parts.length === 2 || !/Bearer$/i.test(scheme) || token === 'undefined' || token.length === 0) {
        let response = errors.headers_not_found
        response.headers = ['Athorization: Bearer "Token"']
        return res.status(response.statusCode).json(response)
    } else {
        /* 
        Verificando se o token fornecido é valido 
         */
        jwt.verify(token, secret, async (error, decoded) => {
            if (error) {
                /*
                o token informado não é válido 
                 */
                response = errors.not_authorized
                return res.status(response.statusCode).json(response)
            } else {
                let user = await User.getOne(decoded.user)              

                if (user.statusCode === 200) {
                    /* 
                    o token é válido e o usuário exite no sistema
                    */
                    req.userId = decoded.user
                    next()
                } else {
                    /* 
                    o token é válido mas, o usuário foi removido do sistema.
                     */
                    let response = errors.user_is_gone
                    return res.status(response.statusCode).json(response)
                }

            }
        })
    }

}