"use strict"
const os = require("os")
/* verificando se o processo recebeu argumentos */
let argv = require("minimist")(process.argv.slice(2))
/* método que retorna todas interfaces e seus IPs */
const getIps = (port) => {
    const ip = require('ip')
    const internalIp = require('internal-ip')
    let ips = ""
    if (internalIp.v4.sync() != null) {
        if (ip.isEqual(internalIp.v4.sync(), ip.address())) {
            ips += `	http: //${ip.address()}:${port}
`
            ips += `	http://${ip.loopback("ipv4")}:${port}`
        }
        else {
            ips += `	http://${ip.address()}:${port}
`
            ips += `	http://${internalIp.v4.sync()}:${port}
`
            ips += `	http://${ip.loopback("ipv4")}:${port}`
        }
    } else {
        if (ip.isEqual(ip.address(), ip.loopback("ipv4"))) {
            ips += `	http://${ip.address()}:${port}
`
        } else {
            ips += `	http://${ip.address()}:${port}
`
            ips += `	http://${ip.loopback("ipv4")}:${port}`
        }
    }

    return ips
}

const setPort = () => {
    return process.env.PORT || 8080
}

/* 
função utilizada para retorna a porta configurada no arquivo /config/default.config
 */
const getPort = () => {
    const {
        server
    } = require("../config/default.config")
    return server.port
}

/* 
função usada para filtrar os items de um objeto,
 retornado um objeto novo com apenas as chaves que foram indicadas por parametro. 
 */
const serialize = (data = [], type = 1, fields = []) => {
    const parseFields = (data, fields) => {
        const newData = {}
        for (const key in fields) {
            newData[fields[key]] = data[fields[key]]
        }
        return newData
    }
    switch (type) {
        case 1:
            return parseFields(data, fields)
            break
        case 2:
            return data.map(newData => {
                return parseFields(newData, fields)
            })
            break
        default:
            return parseFields(data, fields)
            break
    }
}

/* 
gerando o token jwt com tempo de expiração configurado em 24 horas
 */
const generateToken = (params = {}) => {
    const {
        secret
    } = require('../config/secret.json'), jwt = require('jsonwebtoken')
    return jwt.sign(params, secret, {
        expiresIn: '24h'
    })
}
/* Função utilizada pelo upload de arquivo, usada para definir o tipo do arquivo,
 que são: 'Doc', 'Image', 'Video', 'Compress', 'Music', 'Program' e 'Unknow'.*/
const setFileType = (filename) => {
    let typefiles = {
        Doc: ["txt", "doc", "docx", "pdf", "pptx", "odt", "fodt", "ott", "out", "md", "xls", "srt", "html", "htm", "php", "js", "rb", "py", "c", "cpp"],
        Image: ["jpg", "jpeg", "png", "gif", "webp", "ico", "xdm", "bitmap", "bmp", "xpm", "psd", "jpe", "pgm", "pnm", "svg"],
        Video: ["mp4", "mkv", "3gp", "mov", "avi", "wmv", "3g2", "flv", "rm", "mpg", "vob", "rmvb", "webm", "opus", "m3u2", "f4m"],
        Compress: ["zip", "7z", "tar", "xz", "gz", "rar", "bin", "iso", "bz2", "tar"],
        Music: ["mp3", "acc", "m4a", "wav", "wma", "alac", "flac"],
        Program: ["exe", "rpm", "deb", "AppImage", "sh"]
    }
    let types = ['Doc', 'Image', 'Video', 'Compress', 'Music', 'Program']
    let type = ""
    let aux = filename.split('.')
    let ext = aux[aux.length - 1]
    let i = 0

    while (i < types.length) {
        if (typefiles[types[i]].find(tmp => tmp.toString() === ext.toString())) {
            let file = types[i]
            type = `${file[0].toUpperCase()}${file.slice(1)}`
            break
        }
        i++
    }
    if (type.length === 0) {
        type = "Unknow"
    }
    return type
}

const get_file_name = (filename = "", file_names = []) => {
    const get_under_point = (name) => {
        return name.substr(0, name.length - (name.split('.')[name.split('.').length - 1].length + 1))
    }
    const get_under_line = (name) => {
        return name.substr(0, name.length - (name.split('_')[name.split('_').length - 1].length + 1))
    }
    const get_end_under_line = (name) => {
        let tmp = name.split('_')[name.split('_').length - 1]
        let numb = Number(tmp)

        if (Number.isInteger(numb)) {
            return tmp
        }
        return undefined
    }
    const get_ext = (name) => {
        return name.split('.')[name.split('.').length - 1]
    }

    const get_number = (num) => {
        if (num == 1) return num;
        else return num
    }
    let tmp = file_names.filter(name => {
        if (get_under_line(get_under_point(name)).toString() === get_under_point(filename).toString() && get_end_under_line(get_under_point(name)) !== undefined || filename.toString() === name.toString()) {
            return true
        } else {
            return false
        }

    })
    if (tmp.length == 0) {
        return filename
    } else if (filename.split('.').length == 1) {
        return `${filename}_${get_number(tmp.length)}`
    } else {

        return `${get_under_point(filename)}_${get_number(tmp.length)}.${get_ext(filename)}`
    }
}


const remove_file = async (filepath) => {
    const fs = require('fs'),
        path = require('path')
    try {
        fs.unlinkSync(path.resolve(filepath))
        return true
    } catch (error) {
        return false
    }
}

const setNodeENV = () => {
    return process.env.NODE_ENV || 'development'
}

const getEnv = () => {
    return require('../config/default.config').env.NODE_ENV
}

module.exports = {
    getIps,
    getPort,
    setPort,
    serialize,
    generateToken,
    setFileType,
    get_file_name,
    remove_file,
    getEnv,
    setNodeENV,
    config: () => {
        // const path = 
        require('dotenv').config({
            path: 'variables.env'
        })
        setPort()


    }
}
