const {
    setPort,
    setNodeENV
} = require("../utils/index")
module.exports = {
    db: {
        port: 27017,
        url: "localhost",
        name: "teste01",
        /* Esta valor e referente ao modo de conexão com o mongo, em nuvem pode ser feito o uso do mongodb+srv por exemplo */
        mode: "mongodb"
    },
    server: {
        port: setPort()
    },
    secret: "",
    env: {

        "NODE_ENV": setNodeENV()
    }

}