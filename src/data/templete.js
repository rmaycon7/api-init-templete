module.exports = {
    init: {
        author: "",
        description: "",
        keywords: [],
        license: "LGPL-3.0",
        main: "src/server.js",
        name: "exemplo-templete",
        scripts: {
            dev: "NODE_ENV=development nodemon ./src/server.js",
            start: "node ./src/server.js",
            test: "echo \"Error: no test specified\" && exit 1"
        },
        "dependencies": {
            "bcryptjs": "^2.4.3",
            "body-parser": "^1.19.0",
            "compression": "^1.7.4",
            "cors": "^2.8.5",
            "dotenv": "^8.0.0",
            "express": "^4.16.4",
            "helmet": "^3.16.0",
            "internal-ip": "^4.3.0",
            "ip": "^1.1.5",
            "jsonwebtoken": "^8.5.1",
            "mongoose": "^5.5.2",
            "mongoose-bcrypt": "^1.6.0",
            "multer": "^1.4.1",
            "serve-favicon": "^2.5.0",
        },
        "devDependencies": {
            "chalk": "^2.4.2",
            "nodemon": "^1.18.11",
            "portfinder": "^1.0.20"
        },
        version: "0.0.1"
    },
    nodemonConfig: {
        delay: 0,
        env: {
            NODE_ENV: "development"
        },
        events: {
            crash: "touch src/server.js"
        },
        ext: "js json",
        ignore: [
            ".git",
            "doc",
            "node_modules",
            "node_modules/",
            "node_modules/**/node_modules",
            "*-lock.json",
            "*-lock.yaml"
        ],
        restartable: "rs",
        verbose: false,
        watch: [
            "src/"
        ]
    },
    "files": {
        "src_server": {
            name: 'src/server.js',
            data: `"use strict"
const app = require("./app/app"),
    utils = require("./utils"),
    os = require("os")


/* atribuindo o valor da porta em uma constante */
utils.config()
const _port = utils.getPort()

/* 
utilizando um modulo para checkar se a porta escolhida esta disponivel para uso, evitando erro de execução
*/


if (utils.getEnv().toLocaleLowerCase() != 'production') {
    const portfinder = require('portfinder'),
        chalk = require('chalk')

    portfinder.getPort({
        port: _port, // minimum port
        stopPort: _port + 500 // maximum port
    }, (error, port) => {
        if (error) throw error
        else {
            app.listen(port, (error) => {
                console.clear()
                console.log(\`\${chalk.green('\t\tRunning in')}\n\${chalk.red(utils.getIps(port))}\`);
            })

        }
    })
} else {
    app.listen(_port, (error) => {
        console.clear()
        console.log(\`Running in \n\${utils.getIps(_port)}\`);
    })
}`
        },
        "src_app_app": {
            name: "src/app/app.js",
            data: `"use strict"
const bp = require("body-parser"),
    compression = require("compression"),
    cors = require("cors"),
    express = require("express"),
    errorConf = require("../config/default.errors"),
    helmet = require("helmet"),
    app = express()

/* Iniciando a ativação das configurações da aplicação, como a proteção contra ataques, a configuração dos cabeçalhos. */
app.use(bp.json())
app.use(bp.urlencoded({
    extended: true
}))
app.use(cors())
app.use(helmet())
app.use(compression())
const favicon = require('serve-favicon')
const path = require('path')

app.use(favicon(path.join(__dirname, '../../', 'favicon.ico')))
/* A rotas deve ficar a partir daqui
exemplo:
    app.get("/", (req,res, next) => {
        return res.status(202).json({
            msg: "ok"
        })
    })

    ou
    require("./routes")(app)
    neste métodos todos os arquivos de rotas contido no diretório Routes (exceto o index.js que é uma função) serão inportados.
 */

require("./routes/index")(app)
/* Este app.use deve ficar após todas as rotas da aplicação, para o caso de nenhuma rota ser encontrada, ele informará ao requistante dorecurso que este mesmo não existe */
app.use(async (req, res, next) => {
    let error = errorConf.resource_not_found
    error.message = \`Rota \${req.method} para "\${req.path}" não encontrada, por favor verifique a url ou método fornecido e tente novamente.\`
    error.name = \`\${req.method} \${req.path}\`
    return res.status(error.statusCode).json(error)
})
module.exports = app`
        },
        "src_app_middlewares_authorization": {
            name: "src/app/middlewares/authorization.js",
            data: `const jwt = require('jsonwebtoken'),
    {
        secret
    } = require('../../config/secret.json'),
    User = require('../controllers/userController'),
    utils = require('../../utils'),
    errors = require('../../config/default.errors')

module.exports = async (req, res, next) => {
    const header = req.headers.authorization
    let parts
    try {

        parts = header.split(" ")
    } catch (error) {
        parts = ""
    }
    const [scheme, token] = parts
    /* 
    verificando se dentro dos cabeçalhos da requisição contém o tken de autorização, to tipo Bearer token. 
     */
    if (!header || !parts.length === 2 || !/Bearer$/i.test(scheme) || token === 'undefined' || token.length === 0) {
        let response = errors.headers_not_found
        response.headers = ['Athorization: Bearer "Token"']
        return res.status(response.statusCode).json(response)
    } else {
        /* 
        Verificando se o token fornecido é valido 
         */
        jwt.verify(token, secret, async (error, decoded) => {
            if (error) {
                /*
                o token informado não é válido 
                 */
                response = errors.not_authorized
                return res.status(response.statusCode).json(response)
            } else {
                let user = await User.getOne(decoded.user)              

                if (user.statusCode === 200) {
                    /* 
                    o token é válido e o usuário exite no sistema
                    */
                    req.userId = decoded.user
                    next()
                } else {
                    /* 
                    o token é válido mas, o usuário foi removido do sistema.
                     */
                    let response = errors.user_is_gone
                    return res.status(response.statusCode).json(response)
                }

            }
        })
    }

}`
        },
        "src_app_controllers_user_control": {
            name: "src/app/controllers/userController.js",
            data: `"use strict"
const User = require("../models/User.js"),
    utils = require("../../utils"),
    paths = ["email", "id", "name"],
    defaultResponse = (data, code) => {
        if (data) {
            return {
                data: data,
                statusCode: code
            }

        } else {
            return errorResponse(errorConf.user_not_found)
        }
    },
    errorResponse = (error) => {
        return error
    },
    errorConf = require("../../config/default.errors.js")
module.exports = {
    getAll: () => {
        return User.find()
            .then(data => defaultResponse(utils.serialize(data, 2, paths), 200))
            .catch(error => errorResponse(errorConf.users_not_found))
    },
    getOne: (id) => {
        return User.findById(id)
            .then(data => defaultResponse(utils.serialize(data, 1, paths), 200))
            .catch(error => errorResponse(errorConf.user_not_found))
    },
    create: (data) => {
        return User.create(data)
            .then(data => {
                return defaultResponse(utils.serialize(data, 1, paths), 200)

            })
            .catch(error => {
                if (error.code == 11000) {

                    return errorResponse(errorConf.user_already_exists)
                }
                return errorResponse(errorConf.internal_error)
            })
    },
    update: (data, id) => {
        return User.findOneAndUpdate({
                _id: id
            }, data, {
                new: true
            })
            .then(data => defaultResponse(utils.serialize(data, 1, paths), 202))
            .catch(error => errorResponse(errorConf.user_not_found))
    },
    delete: (id) => {
        return User.findOneAndRemove({
                _id: id
            })
            .then(data => defaultResponse(utils.serialize(data, 1, paths), 202))
            .catch(error => errorResponse(errorConf.user_not_found))
    },
    getbyEmail: (email) => {
        return User.find({
                email: email
            })
            .then(data => {
                paths.push("password")
                return defaultResponse(utils.serialize(data[0], 1, paths), 200)
            })
            .catch(error => errorResponse(errorConf.user_not_found))
    },

}`
        },
        "src_app_controllers_auth_controller": {
            name: "src/app/controllers/authController.js",
            data: `"use strict"
const User = require("./userController"),
    utils = require("../../utils"),
    paths = ["email", "id", "name", "password"],
    defaultResponse = (data, code) => {
        if (data) {

            return {
                data: data,
                statusCode: code
            }

        } else {
            return errorResponse(errorConf.user_not_found)
        }
    },
    errorResponse = (error) => {
        return error
    },
    errorConf = require("../../config/default.errors.js"),
    jwt = require('jsonwebtoken'),
    {
        secret
    } = require('../../config/secret.json'),
    bcrypt = require("bcryptjs")

module.exports = {
    login: async (email, password) => {
        let data = await User.getbyEmail(email)
        if (data.statusCode == 200) {
            if (await bcrypt.compare(password, data.data.password)) {
                let token = utils.generateToken({
                    user: data.data.id
                })
                data.token = token
                return data
            } else {
                let {
                    password_wrong
                } = require('../../config/default.errors')
                data.data = password_wrong
                data.statusCode = password_wrong.statusCode
                return data
            }
        } else {
            let {
                user_not_found
            } = require('../../config/default.errors')
            return {
                data: user_not_found,
                statusCode: user_not_found.statusCode
            }
        }
    }
}`
        },
        "src_app_models_user": {
            name: "src/app/models/User.js",
            data: `"use strict"
const mongoose = require("../../database/")
const bcrypt = require("bcryptjs")
const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        bcrypt: true
    }
})

UserSchema.plugin(require("mongoose-bcrypt"), {
    rounds: 10
})

module.exports = mongoose.model("User", UserSchema)`
        },
        "src_app_routes_index": {
            name: "src/app/routes/index.js",
            data: `"use strict"
const fs = require("fs")
const path = require("path")
/* Criando função para import automaticamente todos os arquivos js colocados neste diretório */
module.exports = app => {
    fs
        .readdirSync(__dirname)
        .filter(file => file.indexOf(".") !== 0 && file !== "index.js" && path.extname(file) === ".js")
        .forEach(file => require(path.resolve(__dirname, file))(app))
}
`
        },
        "src_app_routes_user": {
            name: "src/app/routes/userRoutes.js",
            data: `"use strict"
const express = require("express"),
    router = express.Router(),
    userController = require("../controllers/userController"),
    authorization = require('../middlewares/authorization'),
    errors = require('../../config/default.errors')
/*
Rota: Ver todos os usuários
*/
router.get("/", authorization, async (req, res, next) => {
    const response = await userController.getAll()
    return res.status(response.statusCode).json(response)
})
/*
Rota: Ver um usuário
*/
router.get("/:id", authorization, async (req, res, next) => {
    const response = await userController.getOne(req.params.id)
    return res.status(response.statusCode).json(response)
})
/*
Rota: Criar usuário
*/
router.post("/", async (req, res, next) => {
    const response = await userController.create(req.body)
    console.log(response);

    return res.status(response.statusCode).json(response)
})
/*
Rota: Atualizar usuário
*/
router.patch("/:id", authorization, async (req, res, next) => {
    if (req.userId === req.params.id) {
        const response = await userController.update(req.body, req.params.id)
        return res.status(response.statusCode).json(response)
    } else {
        let response = errors.not_authorized
        response.message = 'Id pertence a outro usuário! Tente novamente.'
        response.name = "Id Incorreto!"
        return res.status(response.statusCode).json(response)
    }
})

router.delete("/:id", async (req, res, next) => {
    const response = await userController.delete(req.params.id)
    return res.status(response.statusCode).json(response)
})

module.exports = app => app.use("/users", router)`
        },
        "src_app_routes_auth": {
            name: "src/app/routes/authRoutes.js",
            data: `"use strict"
const express = require("express"),
    router = express.Router(),
    auth = require('../controllers/authController'),
    User = require('../controllers/userController'),
    utils = require('../../utils')
router.post('/login', async (req, res, next) => {
    let {
        email,
        password
    } = req.body
    let response = await auth.login(email, password)
    return res.status(response.statusCode).send(response)
})
router.post('/register', async (req, res, next) => {
    const response = await User.create(req.body)
    if (response.statusCode == 200) {
        response.token = utils.generateToken({
            user: response.data.id
        })
    }
    return res.status(response.statusCode).send(response)
})
module.exports = app => app.use("/", router)`
        },
        "src_database_index": {
            name: "src/database/index.js",
            data: `"use strict"
const mongoose = require("mongoose"),
    {
        db
    } = require("../config/default.config"),
    /* Função que irá se conectar com a base de dados, 
    ira ficar se tentadno se reconectar caso a conexão com banco de dados caia */
    connect = async (mongoose) => {
        try {
            mongoose.Promise = global.Promise
            mongoose.connect(\`\${db.mode}://\${db.url}:\${db.port}/\${db.name}\`)
        } catch (error) {
            setTimeout(() => {
                return con(mongoose)
            }, 2000);
        }
    }

connect(mongoose)
module.exports = mongoose`
        },
        "src_config_config": {
            name: "src/config/default.config.js",
            data: `const {
    setPort,
    setNodeENV
} = require("../utils/index")
module.exports = {
    db: {
        port: 27017,
        url: "localhost",
        name: "teste01",
        /* Esta valor e referente ao modo de conexão com o mongo, em nuvem pode ser feito o uso do mongodb+srv por exemplo */
        mode: "mongodb"
    },
    server: {
        port: setPort()
    },
    secret: "",
    env: {

        "NODE_ENV": setNodeENV()
    }

}`
        },
        "src_config_errors": {
            name: "src/config/default.errors.js",
            data: `/* Alguns erros muito comuns, como campos (fields )faltando, erro interno no servidor, etc ...
Para aginilar nas repostas, bastando apenas importar este arquivo e usar os valoeres nele contido.*/
module.exports = {
    fields_not_found: {
        code: "FIELDS_NULL",
        fields: [],
        message: "Estes campos são obrigatórios, preencha os campos e tente novamente.",
        name: "Cadastrar ou atualizar dados.",
        statusCode: 428
    },
    headers_not_found: {
        code: "HEADERS_NOT_FOUND",
        headers: [],
        message: "Estes cabeçalhos são obrigatórios, preencha os cabeçalhos e tente novamente.",
        name: "Cabeçalhos incorretos",
        statusCode: 412
    },
    internal_error: {
        code: "INTERNAL_ERROR",
        message: "Erro interno no servidor, por favor tente novamente.",
        name: "Erro interno",
        statusCode: 500
    },
    not_authorized: {
        code: "AUTHORIZATION_REQUIRED",
        message: "Autorização Necessária, Token de autênticação válido é requerido.",
        name: "Token inválido",
        statusCode: 401
    },
    password_wrong: {
        code: "PASSWORD_INCORRECT",
        message: "Senhas não conferem, corriga e tente novamente",
        name: "Error",
        statusCode: 401
    },
    resource_not_found: {
        code: "ROUTE_INCORRECT",
        message: "",
        name: "",
        statusCode: 404
    },
    unauthorized: {
        code: "NOT_AUTHORIZED",
        message: "Usuário não esta autorizado a acessar esses dados.",
        name: "Vizualizar dados do usuário.",
        statusCode: 401
    },
    user_already_exists: {
        code: "USER_EXISTS",
        message: "Usuário já existe, mude o email e tente novamente.",
        name: "Error",
        statusCode: 409
    },
    user_not_found: {
        code: "USER_ID_INCORRECT",
        message: "Usuário não encontrado, verifique o Id fornecido e tente novamente.",
        name: "Usuário nã encontrado",
        statusCode: 404
    },
    users_not_found: {
        code: "NO_USERS_FOUND",
        message: "Nenhum usuário encontrado, tente novamente.",
        name: "No Users Found",
        statusCode: 404
    },

    user_is_gone: {
        code: "USER_IS_GONE",
        message: "O usuário não existe mais, tente novamente.",
        name: "User removed",
        statusCode: 410
    },
    file_not_found: {
        code: 'FILE_NOT_FOUND',
        message: "Arquivo não encontrado, verifique o id fornecido e tente novamente.",
        name: "Download de Arquivo",
        statusCode: 404
    }

}`
        },
        "src_utils_index": {
            name: 'src/utils/index.js',
            data: `"use strict"
const os = require("os")

/* método que retorna todas interfaces e seus IPs */
const getIps = (port) => {
    const ip = require('ip')
    const internalIp = require('internal-ip')
    let ips = ""
    if (internalIp.v4.sync() != null) {
        if (ip.isEqual(internalIp.v4.sync(), ip.address())) {
            ips += \`\thttp: //\${ip.address()}:\${port}\n\`
            ips += \`\thttp://\${ip.loopback("ipv4")}:\${port}\`
        }
        else {
            ips += \`\thttp://\${ip.address()}:\${port}\n\`
            ips += \`\thttp://\${internalIp.v4.sync()}:\${port}\n\`
            ips += \`\thttp://\${ip.loopback("ipv4")}:\${port}\`
        }
    } else {
        if (ip.isEqual(ip.address(), ip.loopback("ipv4"))) {
            ips += \`\thttp://\${ip.address()}:\${port}\n\`
        } else {
            ips += \`\thttp://\${ip.address()}:\${port}\n\`
            ips += \`\thttp://\${ip.loopback("ipv4")}:\${port}\`
        }
    }

    return ips
}

const setPort = () => {
    return process.env.PORT || 8080
}

/* 
função utilizada para retorna a porta configurada no arquivo /config/default.config
 */
const getPort = () => {
    const {
        server
    } = require("../config/default.config")
    return server.port
}

/* 
função usada para filtrar os items de um objeto,
 retornado um objeto novo com apenas as chaves que foram indicadas por parametro. 
 */
const serialize = (data = [], type = 1, fields = []) => {
    const parseFields = (data, fields) => {
        const newData = {}
        for (const key in fields) {
            newData[fields[key]] = data[fields[key]]
        }
        return newData
    }
    switch (type) {
        case 1:
            return parseFields(data, fields)
            break
        case 2:
            return data.map(newData => {
                return parseFields(newData, fields)
            })
            break
        default:
            return parseFields(data, fields)
            break
    }
}

/* 
gerando o token jwt com tempo de expiração configurado em 24 horas
 */
const generateToken = (params = {}) => {
    const {
        secret
    } = require('../config/secret.json'), jwt = require('jsonwebtoken')
    return jwt.sign(params, secret, {
        expiresIn: '24h'
    })
}
/* Função utilizada pelo upload de arquivo, usada para definir o tipo do arquivo,
 que são: 'Doc', 'Image', 'Video', 'Compress', 'Music', 'Program' e 'Unknow'.*/
const setFileType = (filename) => {
    let typefiles = {
        Doc: ["txt", "doc", "docx", "pdf", "pptx", "odt", "fodt", "ott", "out", "md", "xls", "srt", "html", "htm", "php", "js", "rb", "py", "c", "cpp"],
        Image: ["jpg", "jpeg", "png", "gif", "webp", "ico", "xdm", "bitmap", "bmp", "xpm", "psd", "jpe", "pgm", "pnm", "svg"],
        Video: ["mp4", "mkv", "3gp", "mov", "avi", "wmv", "3g2", "flv", "rm", "mpg", "vob", "rmvb", "webm", "opus", "m3u2", "f4m"],
        Compress: ["zip", "7z", "tar", "xz", "gz", "rar", "bin", "iso", "bz2", "tar"],
        Music: ["mp3", "acc", "m4a", "wav", "wma", "alac", "flac"],
        Program: ["exe", "rpm", "deb", "AppImage", "sh"]
    }
    let types = ['Doc', 'Image', 'Video', 'Compress', 'Music', 'Program']
    let type = ""
    let aux = filename.split('.')
    let ext = aux[aux.length - 1]
    let i = 0

    while (i < types.length) {
        if (typefiles[types[i]].find(tmp => tmp.toString() === ext.toString())) {
            let file = types[i]
            type = \`\${file[0].toUpperCase()}\${file.slice(1)}\`
            break
        }
        i++
    }
    if (type.length === 0) {
        type = "Unknow"
    }
    return type
}

const get_file_name = (filename = "", file_names = []) => {
    const get_under_point = (name) => {
        return name.substr(0, name.length - (name.split('.')[name.split('.').length - 1].length + 1))
    }
    const get_under_line = (name) => {
        return name.substr(0, name.length - (name.split('_')[name.split('_').length - 1].length + 1))
    }
    const get_end_under_line = (name) => {
        let tmp = name.split('_')[name.split('_').length - 1]
        let numb = Number(tmp)

        if (Number.isInteger(numb)) {
            return tmp
        }
        return undefined
    }
    const get_ext = (name) => {
        return name.split('.')[name.split('.').length - 1]
    }

    const get_number = (num) => {
        if (num == 1) return num;
        else return num
    }
    let tmp = file_names.filter(name => {
        if (get_under_line(get_under_point(name)).toString() === get_under_point(filename).toString() && get_end_under_line(get_under_point(name)) !== undefined || filename.toString() === name.toString()) {
            return true
        } else {
            return false
        }

    })
    if (tmp.length == 0) {
        return filename
    } else if (filename.split('.').length == 1) {
        return \`\${filename}_\${get_number(tmp.length)}\`
    } else {

        return \`\${get_under_point(filename)}_\${get_number(tmp.length)}.\${get_ext(filename)}\`
    }
}


const remove_file = async (filepath) => {
    const fs = require('fs'),
        path = require('path')
    try {
        fs.unlinkSync(path.resolve(filepath))
        return true
    } catch (error) {
        return false
    }
}

const setNodeENV = () => {
    return process.env.NODE_ENV || 'development'
}

const getEnv = () => {
    return require('../config/default.config').env.NODE_ENV
}

module.exports = {
    getIps,
    getPort,
    setPort,
    serialize,
    generateToken,
    setFileType,
    get_file_name,
    remove_file,
    getEnv,
    setNodeENV,
    config: () => {
        // const path = 
        require('dotenv').config({
            path: 'variables.env'
        })
        setPort()


    }
}
`
        },
        "variables_env": {
            name: 'variables.env',
            data: `PORT=9090
NODE_ENV=development
# descomente a proxima linha para rodar em produção
# NODE_ENV=production
`
        },
        "secret": {
            name: "src/config/secret.json",
            data: `{
                "secret": "asdasdasdasdasdabdhd7sd8agd8asd9"
            }`
        },
        "src_app_controllers_file": {

            "name": "src/controllers/fileController.js",
            "data": `"use-strict"
const express = require('express'),
    fs = require('fs'),
    path = require('path'),
    File = require('../models/File'),
    utils = require("../../utils"),
    errors = require('../../config/default.errors')


const list = async () => {
    try {
        const data = await File.find().sort({
            originalname: 1
        });
        if (data.length == 0) {
          
            return {
                data: [],
                statusCode: 200
            };
        } else {
            return {
                statusCode: 200,
                data: utils.serialize(data, 2, ['id', 'originalname', 'size', 'type'])
            };
        }
    } catch (error) {
        return errors.internal_error;
    }
}, listbytype = async (type) => {
    try {
        

        const data = await File.find({
            type: type
        });

        if (data.length == 0) {

            return {
                data: [],
                statusCode: 200
            };
        } else {
            return {
                statusCode: 200,
                data: utils.serialize(data, 2, ['id', 'originalname', 'size', 'type'])
            };
        }
    } catch (error) {
        return errors.internal_error;
    }
}, create = async (files = []) => {
        const Data = {
            Doc: {
                data
            } = await listbytype('Doc'),
            Compress: {
                data
            } = await listbytype('Compress'),
            Image: {
                data
            } = await listbytype('Image'),
            Program: {
                data
            } = await listbytype('Program'),
            Video: {
                data
            } = await listbytype('Video'),
            Unknow: {
                data
            } = await listbytype('Unknow'),
            Music: {
                data
            } = await listbytype('Music')
        }
        files.map(async (file) => {
         

            file.type = utils.setFileType(file.originalname)
         
            file.originalname = utils.get_file_name(file.originalname, Data[file.type].data.map(file => {
                return file.originalname
            }))
            file.path = file.destination
            file.name = file.filename
           
            return file
        });
        try {
            let data = await File.insertMany(files)
            let dados = utils.serialize(data, 2,
                ['id', 'originalname', 'size', 'type']
            )

            return {
                data: dados,
                statusCode: 200
            }

        } catch (error) {

            return errors.internal_error
        }
        
    },
    download = async (id = '') => {
            try {
                let data = await File.findById(id)
              
                if (data) {
                    return {
                        data: data,
                        statusCode: 200
                    }
                } else {
                    throw Error
                }
            } catch (error) {
                return errors.file_not_found
            }
        },
        deletefile = async (id = '') => {
            try {
                let data = await File.findByIdAndDelete(id)
                
                if (data) {
                    return {
                        data: data,
                        statusCode: 200
                    }
                } else {
                    throw Error
                }
            } catch (error) {
                return errors.file_not_found
            }
        }




module.exports = {
    List: list,
    ListByType: listbytype,
    Create: create,
    Download: download,
    Delete: deletefile

}`
        },
        "src_app_models_file": {
            "name": "src/models/File.js",
            "data": `"use strict"
const mongoose = require("../../database/")

const FileSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    originalname: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    size: {
        type: String,
        required: true
    },
    mimetype: {
        type: String,
        required: true
    }
})


module.exports = mongoose.model("File", FileSchema)`
        },
        "src_app_routes_files": {
            "name": "src/app/routes/fileRoutes.js",
            "data": `"use strict"
const express = require("express"),
    router = express.Router(),
    Files = require("../controllers/fileController"),
    multer = require('multer'),
    upload = multer({
        dest: "uploads/"
    }).array("files", 200),
    utils = require('../../utils'),
    path = require('path')



router.get('/', async (req, res, next) => {
    const response = await Files.List()
    return res.status(response.statusCode).json(response)
})


router.post('/', async (req, res, next) => {
    await upload(req, res, async error => {

        const response = await Files.Create(req.files)
      
        return res.status(response.statusCode).json(response)

    })
})
/* 
rota para listar arquivos pelo tipo
route to list files by type
 */

router.get('/:type', async (req, res, next) => {
    let types = ['Doc', 'Image', 'Video', 'Compress', 'Music', 'Program', 'Unknow']
    if (types.find(type => type.toString() === req.params.type.toString())) {
        const response = await Files.ListByType(req.params.type)
        return res.status(response.statusCode).json(response)

    } else {
        next()
    }

})
/* 
rota para baixar um arquivo pelo id
route to download a file by id
 */
router.get('/:id', async (req, res, next) => {
    const response = await Files.Download(req.params.id)
    if (response.statusCode == 200) {
        let options = {
            root: response.data.path,
            dotfiles: "deny",
            headers: {
                "x-timestamp": Date.now(),
                "x-sent": true
            },
            acceptRanges: true
        };
        res.attachment(response.data.originalname)
        return res.status(200).sendFile(response.data.name, options)
    } else {
        return res.status(response.statusCode).json(response)
    }
})
router.delete('/:id', async (req, res, next) => {
    const response = await Files.Delete(req.params.id)
    if (response.statusCode == 200) {
        let {
            name,
            path
        } = response.data
        

        await utils.remove_file(\`\${path}\${name}\`)
        return res.status(201).json({
            statusCode: 201,
            message: 'Arquivo deletado.'
        })
    } else {
        return res.status(response.statusCode).json(response)
    }
})
module.exports = app => app.use("/files", router)`
        }

    }


}