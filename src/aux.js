#!/usr/bin/env node

const path = require('path')
const fs = require('fs')
const readline = require('readline');
const inquirer = require('inquirer');

const chalk = require('chalk');
const clear = require('clear');
// clear();

chalk.enabled
chalk.level = 10
// const readline = require('readline')
// const blank = '\n'.repeat(process.stdout.rows)
// console.log(blank)
// readline.cursorTo(process.stdout, 0, 0)
// readline.clearScreenDown(process.stdout)
// console.log(chalk.red('Hello world!') + ' 🛠️ ');
const {
    exec
} = require('child_process')
const cwd = path.resolve('.')
// console.log({
//     path: cwd
// });
// try {
//     // exec('pnpm add ora --offline', (error, stout, stdin) => {
//     exec('./src/teste.js', {
//         cwd: cwd
//     }, (error, stout, stdin) => {
//         console.log({
//             error: error,
//             stdut: stout
//         });
//     })
// } catch (error) {

// }




const execa = require('execa');
const portfinder = require('portfinder');
let apiMode = false
let command = 'yarn';

// (async () => {

//     await execa('src/teste.js', [''], {
//         // stdio: ['inherit', apiMode ? 'pipe' : 'inherit', !apiMode && command === 'yarn' ? 'pipe' : 'inherit']
//         stdio: ['inherit', apiMode ? 'pipe' : 'inherit', !apiMode && command === 'yarn' ? 'pipe' : 'inherit']
//     }) //.stdout.pipe(process.stdout);
//     console.log('from async ');

// });
// (async () => {
//     const child = execa('pnpm', ['add', 'express', 'mongoose', 'helmet', 'vuex', 'axios', 'bootstrap', '-D', '--offline'], {
//         stdio: ['inherit', 'inherit', 'pipe']
//         // stdio: ['inherit', apiMode ? 'pipe' : 'inherit', !apiMode && command === 'yarn' ? 'pipe' : 'inherit']

//     })
//     console.log({
//         stdio: ['inherit', apiMode ? 'pipe' : 'inherit', !apiMode && command === 'yarn' ? 'pipe' : 'inherit']
//     });

//     // .stdout.pipe(process.stdout);
//     // console.log(stdout);
//     //=> 'unicorns'
//     let progressTotal = 0
//     let progressTime = Date.now()
//     try {

//         child.stdout.on('data', buffer => {
//             process.stdout.write(buffer)
//         })
//     } catch (error) {

//     }
//     // // let str = buffer.toString().trim()
//     // // let a = true
//     // // // if (str && str.indexOf('"type":') !== -1) {
//     // // if (a) {
//     // //     const newLineIndex = str.lastIndexOf('\n')
//     // //     if (newLineIndex !== -1) {
//     // //         str = str.substr(newLineIndex)
//     // //     }
//     // //     try {
//     // //         const data = JSON.parse(str)
//     // //         if (data.type === 'step') {
//     // //             progress.enabled = false
//     // //             progress.log(data.data.message)
//     // //         } else if (data.type === 'progressStart') {
//     // //             progressTotal = data.data.total
//     // //         } else if (data.type === 'progressTick') {
//     // //             const time = Date.now()
//     // //             if (time - progressTime > 20) {
//     // //                 progressTime = time
//     // //                 progress.progress = data.data.current / progressTotal
//     // //             }
//     // //         } else {
//     // //             progress.enabled = false
//     // //         }
//     // //     } catch (e) {
//     // //         console.error(e)
//     // //         console.log(str)
//     // //     }
//     // // } else {
//     // let str = buffer.toString().trim()
//     // // console.log({
//     // //     str: str
//     // // });
//     // process.stdout.write(`\r${buffer}`)
//     // // }
//     console.log();
// })();

// console.log(chalk.supportsColor);
// chalk.default
// for (let i = 0; i < 100; i++) {
//     // const element = array[i];
//     // console.log(`teste ${i}\r`)
//     process.stdout.write(`\r${i}`)
//     // console.
// }
// console.log()



const program = require('commander');

// program
//     .version('0.1.0', '-v, --version')
//     .command('install [name]', 'install one or more packages')
//     .command('search [query]', 'search with optional query')
//     .command('list', 'list packages installed', {
//         isDefault: true
//     })
//     .parse(process.argv);



//  portfinder.getPortPromise()
//      .then((port) => {
//          //
//          // `port` is guaranteed to be a free port
//          // in this scope.
//          //
//      })
//      .catch((err) => {
//          //
//          // Could not get a free port, `err` contains the reason.
//          //
//      });


let data = fs.readFileSync(path.resolve(__dirname, '../variables.env'), {
    encoding: 'utf8'
})

let novo = ``
console.log({
    data: data,
    string: data.toString(),
    trim: data.toString().trim(),
    // json: JSON.parse
});


// const {
//     src_server,
//     src_app_app,
// } = require('./data/templete.js')

const files = require('./data/templete').files

// console.log({
//     server: src_server
// });
const mkdir = require('./utils/index').mkdir
const mkFile = require('./utils/index').mkFile
// for (i in files) {
//     // ''.substring
//     // filepath = files[i].name.substring(0, files[i].split('/')[])
//     let file = files[i]
//     let filepath = path.dirname(files[i].name)
//     let fileabspath = path.resolve(__dirname, 'test', file.name)
//     // path.
//     mkdir(path.resolve(__dirname, 'test', filepath))
//     mkFile(fileabspath, file.data)
//     // console.log(`${path.dirname(filepath)}`);
//     // console.log(`${path.basename(path.resolve(__dirname, filepath) )}`);
//     // fs.writeFileSync(path.resolve(__dirname, `test/${i}.js`), files[i])
// }





// const inquirer = require('..');
questions = [{
        type: 'confirm',
        name: 'toBeDelivered',
        message: 'Is this for delivery?',
        default: false
    },
    {
        type: 'input',
        name: 'phone',
        message: "What's your phone number?",
        validate: function (value) {
            var pass = value.match(
                /^([01]{1})?[-.\s]?\(?(\d{3})\)?[-.\s]?(\d{3})[-.\s]?(\d{4})\s?((?:#|ext\.?\s?|x\.?\s?){1}(?:\d+)?)?$/i
            );
            if (pass) {
                return false;
            }
            return true
            // return 'Please enter a valid phone number';
        }
    },
    {
        type: 'list',
        name: 'size',
        message: 'What size do you need?',
        choices: ['Large', 'Medium', 'Small'],
        filter: function (val) {
            return val.toLowerCase();
        }
    },
    {
        type: 'input',
        name: 'quantity',
        message: 'How many do you need?',
        validate: function (value) {
            var valid = !isNaN(parseFloat(value));
            return valid || 'Please enter a number';
        },
        filter: Number
    },
    {
        type: 'expand',
        name: 'toppings',
        message: 'What about the toppings?',
        choices: [{
                key: 'p',
                name: 'Pepperoni and cheese',
                value: 'PepperoniCheese'
            },
            {
                key: 'a',
                name: 'All dressed',
                value: 'alldressed'
            },
            {
                key: 'w',
                name: 'Hawaiian',
                value: 'hawaiian'
            }
        ]
    },
    {
        type: 'rawlist',
        name: 'beverage',
        message: 'You also get a free 2L beverage',
        choices: ['Pepsi', '7up', 'Coke']
    },
    {
        type: 'input',
        name: 'comments',
        message: 'Any comments on your purchase experience?',
        default: 'Nope, all good!'
    },
    {
        type: 'list',
        name: 'prize',
        message: 'For leaving a comment, you get a freebie',
        choices: ['cake', 'fries'],
        when: function (answers) {
            return answers.comments !== 'Nope, all good!';
        }
    }
];

// inquirer.prompt(questions).then(answers => {
//     console.log('\nOrder receipt:');
//     console.log(JSON.stringify(answers, null, '  '));
// });





// chalk.

inquirer
    .prompt([{
        type: 'list',
        name: 'initchoices',
        message: 'Opções de configuração:',
        choices: [{
                name: `default: ${chalk.yellow('(')}${chalk.bold.green('pastas, arquivos e modulos')}${chalk.yellow(')')}`
            },
            `full: ${chalk.yellow('(')}${chalk.bold.green('pastas, arquivos,  modulos, configuração e upload de arquivos')}${chalk.yellow(')')}`,
            `manual: ${chalk.bold.green('(')}${chalk.white('Escolha quais funcionalidades deja.')}${chalk.bold.green(')')}`
        ]
    }])
    .then(answers => {
        console.log({
            c: answers.initchoices.split(':')[0]
        })
        console.log(JSON.stringify(answers, null, '  '));
    })


// inquirer
//     .prompt([{
//             type: 'list',
//             name: 'initchoices',
//             message: '',
//             choices: [
//                 `Order a pizza ${chalk.red('(')}${chalk.bold.yellow('babel')}${chalk.red(')')}`,
//                 'Make a reservation',
//                 new inquirer.Separator(),
//                 'Ask for opening hours',
//                 {
//                     name: 'Contact support',
//                     disabled: 'Unavailable at this time'
//                 },
//                 'Talk to the receptionist'
//             ]
//         },
//         {
//             type: 'list',
//             name: 'size',
//             message: 'What size do you need?',
//             choices: ['Jumbo', 'Large', 'Standard', 'Medium', 'Small', 'Micro'],
//             filter: function (val) {
//                 return val.toLowerCase();
//             }
//         }
//     ])
//     .then(answers => {
//         console.log(JSON.stringify(answers, null, '  '));
//     });








// inquirer
//     .prompt([{
//         type: 'expand',
//         message: 'Conflict on `file.js`: ',
//         name: 'overwrite',
//         choices: [{
//                 key: 'y',
//                 name: 'Overwrite',
//                 value: 'overwrite'
//             },
//             {
//                 key: 'a',
//                 name: 'Overwrite this one and all next',
//                 value: 'overwrite_all'
//             },
//             {
//                 key: 'd',
//                 name: 'Show diff',
//                 value: 'diff'
//             },
//             new inquirer.Separator(),
//             {
//                 key: 'x',
//                 name: 'Abort',
//                 value: 'abort'
//             }
//         ]
//     }])
//     .then(answers => {
//         console.log(JSON.stringify(answers, null, '  '));
//     });

// inquirer
//     .prompt([{
//         type: 'checkbox',
//         message: 'Select toppings',
//         name: 'toppings',
//         choices: [
//             new inquirer.Separator(''), {
//                 name: 'Pepperoni'
//             }, {
//                 name: 'Ham'
//             }, {
//                 name: 'Ground Meat'
//             }, {
//                 name: 'Bacon'
//             },
//             new inquirer.Separator(' = The Cheeses = '), {
//                 name: 'Mozzarella',
//                 checked: true
//             }, {
//                 name: 'Cheddar'
//             }, {
//                 name: 'Parmesan'
//             },
//             new inquirer.Separator(' = The usual ='), {
//                 name: 'Mushroom'
//             }, {
//                 name: 'Tomato'
//             },
//             new inquirer.Separator(' = The extras = '), {
//                 name: 'Pineapple'
//             }, {
//                 name: 'Olives',
//                 disabled: 'out of stock'
//             }, {
//                 name: 'Extra cheese'
//             }
//         ],
//         validate: function (answer) {
//             if (answer.length < 1) {
//                 return 'You must choose at least one topping.';
//             }

//             return true;
//         }
//     }])
//     .then(answers => {
//         console.log(JSON.stringify(answers, null, '  '));
//     });








// fs.writeFileSync(path.resolve(__dirname, 'test/sever.js'), src_server)
// fs.writeFileSync(path.resolve(__dirname, 'test/app/app.js'), src_app_app)
// portfinder.getPort({
//     port: 3000, // minimum port
//     stopPort: 3333 // maximum port
// }, (error, port) => {
//     console.log({
//         error: error,
//         port: port
//     });
// });

const internalIp = require('internal-ip');
const ip = require('ip');
// (async () => {
//     console.log(await internalIp.v6());
//     //=> 'fe80::1'

//     console.log(await internalIp.v4());
//     //=> '10.0.0.79'
// })();

// // console.log(internalIp.v6.sync())
// //=> 'fe80::1'

// console.log({
//     ip: internalIp.v4.sync(),
//     ip_ip: ip.address()
// })
//=> '10.0.0.79'



// const rl = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout,
//     // terminal: false
// });

// rl.question('What do you think of Node.js? ', (answer) => {
//     // TODO: Log the answer in a database
//     console.log(`Thank you for your valuable feedback: ${answer}`);

//     rl.close();
//     rl.question('What do you think of Node.js? ', (answer) => {
//         // TODO: Log the answer in a database
//         console.log(`Thank you for your valuable feedback: ${answer}`);

//         rl.close();
//         rl.question('What do you think of Node.js? ', (answer) => {
//             // TODO: Log the answer in a database
//             console.log(`Thank you for your valuable feedback: ${answer}`);

//             rl.close();
//         });
//     });
// });
/**
 * Criar Diretorio 
 */

// try {
//     fs.mkdirSync('', {
//         recursive: true
//     })
//     console.info("created")
// } catch (error) {
//     console.info("Exists")
// }
// path.

// console.info({
//     msg: "ok"
// })